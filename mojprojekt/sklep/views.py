from django.shortcuts import HttpResponse, get_object_or_404
from django.shortcuts import render
from .models import Product, Order, Complaint, OrderedProduct, Coupon, Opinion
from django.http import HttpResponseRedirect
from .forms import OrderForm, ComplaintForm, OpinionForm


def index(request, imie):
    return render (request,
                   "sklep/glowna.html",
                   {"imie_klienta": imie})


def product_details(request, prod_id):
    prod =Product.objects.get(id=prod_id)
    if request.method == 'POST':
        form = OpinionForm(request.POST)
        if form.is_valid():
            opinion = Opinion(
                ocena=form.cleaned_data['ocena'],
                autor=form.cleaned_data['autor'],
                opinia=form.cleaned_data['opinia'],
                product=prod
                )
            opinion.save()
            return HttpResponseRedirect('/product/'+str(prod_id))
    else:
        form = OpinionForm()

    opinions=prod.get_opinions()
    context = {'product': prod, 'form':form, 'opinions':opinions}
    return render(request, "sklep/details.html", context)



def product_list(request):
    products =Product.objects.order_by('id')
    context = {'products': products}
    return render(
        request,
        "sklep/list.html",
        context
    )
def order_details(request, order_id):
    order = get_object_or_404(Order, pk=order_id)
    return render(
        request,
        "sklep/order.html",
        {'total_price': order.get_total_price(),
         'products': order.get_products(),
         'final_cost': order.get_final_price(),
         'discount': order.coupon}
    )


def order(request):
    products_to_order = _get_products_in_cart(request)
    if request.method == 'POST':
        form = OrderForm(request.POST)

        if form.is_valid():
            order = Order(
                name=form.cleaned_data['name'],
                address=form.cleaned_data['address'],
                delivery=form.cleaned_data['delivery'],
                coupon=Coupon.objects.filter(code=form.cleaned_data['code']).first()
                )

            order.save()
            for product in products_to_order:
                    OrderedProduct(
                        product=product, order=order, amount=1
                    ).save()
            request.session['cart'] = []
            return HttpResponseRedirect('/order/'+str(order.id))
    else:
        form = OrderForm()
    return render(request, "sklep/order_form.html", {"form": form,"products": products_to_order})


def complaint(request):
    if request.method == 'POST':
        form = ComplaintForm(request.POST)
        if form.is_valid():
            complaint = Complaint(
                name=form.cleaned_data['name'],
                message=form.cleaned_data['message']
                )
            complaint.save()
            return HttpResponseRedirect('/complaint/'+str(complaint.id))
    else:
        form = ComplaintForm()
    return render(request, "sklep/complaint_form.html", {"form": form})


def add_opinion(request, prod_id):
    if request.method == 'POST':
        form = OpinionForm(request.POST)
        if form.is_valid():
            opinion = Opinion(
                rating=form.cleaned_data['rating'],
                comment=form.cleaned_data['comment']
                )
            opinion.save()
            return HttpResponseRedirect('/product/'+str(prod_id))
    else:
        form = OpinionForm()
    return render(request, "sklep/details.html", {"form2": form})


def complaint_details(request, complaint_id):
    complaint = get_object_or_404(Complaint, pk=complaint_id)
    context = {'complaint':complaint}
    return render(
        request,
        "sklep/complaint.html",
        context
    )


def add_to_cart(request):
    if request.method == "POST":
        if 'cart' not in request.session:
             request.session['cart'] = []
        request.session['cart'].append(request.POST['item_id'])
        request.session.modified = True
    return HttpResponseRedirect('/cart')


def remove_from_cart(request):
    if request.method == "POST":
        request.session['cart'].remove(request.POST['item_id'])
        request.session.modified = True
    return HttpResponseRedirect('/cart')


def change_number(request):
    if request.method == "POST":
        request.session['cart'].append(request.POST['item_id'])
        request.session.modified = True
        print(request.POST.get('quantity_field'))
    return HttpResponseRedirect('/cart')


def _get_products_in_cart(request):
    products_in_cart = []
    for item_id in request.session.get('cart', []):
        products_in_cart.append(Product.objects.get(pk=item_id))
    return products_in_cart



def cart(request):
    products_in_cart = _get_products_in_cart(request)
    cart_total=0
    for prod in products_in_cart:
        cart_total+=prod.price
    return render(request, "sklep/cart.html", {"cart_total": cart_total, "counted": count_products(products_in_cart)})


def count_products(products):
    count_dict = {}

    for p in products:
        if p not in count_dict:
            count_dict[p] = 0
        count_dict[p]+=1
    print(count_dict)
    return count_dict

