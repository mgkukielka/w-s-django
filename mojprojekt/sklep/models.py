from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=100)
    price = models.FloatField(default=0)
    description = models.CharField(default='brak opisu', max_length=500)
    weight = models.FloatField(default=0)

    def __str__(self):
        return self.name

    def add_opinion(self, comment, rating):
        self.opinions.append([comment, rating])

    def get_opinions(self):
        opinions = []
        for opinia in Opinion.objects.filter(product=self):
            opinions.append(opinia)
        return opinions

    def get_srednia_ocen(self):
        oceny=[]
        opinie=self.get_opinions()
        for o in opinie:
            oceny.append(o.ocena)
        return round(sum(oceny)/len(oceny),2)


class Opinion(models.Model):
    CHOICES = ((1,"1"),(2,"2"),(3,"3"),(4,"4"),(5,"5"),(6,"6"),(7,"7"),(7,"8"),(9,"9"),(10,"10"))

    autor=models.CharField(max_length=80, default="")
    opinia= models.CharField(max_length=300)
    ocena=models.IntegerField(default=1, choices= CHOICES)
    product=models.ForeignKey(Product, on_delete=models.CASCADE, default=None)

    def __str__(self):
        return self.opinia
2

class Coupon(models.Model):
    name = models.CharField(max_length=150, null=True)
    code = models.CharField(max_length=100, default="")
    discount = models.FloatField(default=0)

    def __str__(self):
        return self.code


class Order(models.Model):

    def __str__(self):
        return self.name

    def get_total_price(self):
        total = 0

        ordered_products = OrderedProduct.objects.filter(order=self)
        for ordered_product in ordered_products:
            total += ordered_product.amount * ordered_product.product.price
        return total

    def get_final_price(self):
        total= self.get_total_price()
        if self.coupon:
            total = round(total * ((100 - int(self.coupon.discount)) / 100), 2)
        return total

    def get_products(self):
        return OrderedProduct.objects.filter(order=self);

    name=models.CharField(max_length=64, null=True)
    address=models.CharField(max_length=128, null=True)
    delivery=models.CharField(max_length=64, null=True)
    ordered_products=models.ManyToManyField("Product", through="OrderedProduct")
    code=models.CharField(max_length=100, blank=True)
    coupon=models.ForeignKey(Coupon, on_delete=models.PROTECT, null=True)


class OrderedProduct(models.Model):
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    amount = models.IntegerField(default=1)



class Complaint(models.Model):

    def __str__(self):
        return self.name

    name = models.CharField(max_length=64, null=True)
    message = models.CharField(max_length=256, null=True)


