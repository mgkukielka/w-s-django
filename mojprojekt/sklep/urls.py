from django.urls import path
from . import views
urlpatterns = [
    path('glowna/', views.index),
    path('glowna/<str:imie>', views.index),
    path("products/", views.product_list),
    path("product/<int:prod_id>/add_opinion", views.add_opinion),
    path("product/<int:prod_id>", views.product_details),
    path("order/<int:order_id>", views.order_details),
    path("order/", views.order),
    path("cart/add/", views.add_to_cart),
    path("cart/remove/", views.remove_from_cart),
    path("cart/change_number/", views.change_number),
    path("cart/", views.cart),
    path("complaint/", views.complaint),
    path("complaint/<int:complaint_id>", views.complaint_details),
]